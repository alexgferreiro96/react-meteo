import styled from "styled-components";

const { Row, Container } = require("reactstrap")

const ContainerTiempo = styled.div`
    border: 1px solid black;
    margin: 5px;
`;
const ItemTiempo = (props) => {
    return(
        <ContainerTiempo>
            <Container className="text-center">
                <Row className={"justify-content-center"}>
                    {props.dia}
                </Row>
                <Row className={"justify-content-center"}>
                    <img src={props.urlImg} alt="icon" />
                </Row>
                <Row className={"justify-content-center"}>
                    <p className={props.temp > 15 ? "text-danger" : "text-primary"}>{props.temp}º</p>
                </Row>        
            </Container>    
        </ContainerTiempo>
    )
}
export default ItemTiempo;