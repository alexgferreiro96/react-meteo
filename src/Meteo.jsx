import React, { useState, useEffect } from "react";

import { Col, Container, Row } from "reactstrap";
import styled from "styled-components";
import ItemTiempo from "./ItemTiempo";

const IconoCentrado = styled.i`
  position: absolute;
  left: 0;
  right: 0;
  top: 200px;
  width: 100%;
`;

const Meteo = () => {
  const [llista, setLlista] = useState([]);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const ciutat = "granada,es";
    const apiKey = "84dbcf8c3480649bce9d4bb58da44b4e";
    const funcio = "forecast";
    const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;
    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        setLlista(data.list);
        setLoading(false);
      })
      .catch((error) => setError(true));
  }, []);

  if (error) {
    return <h3>Se ha producido un error...</h3>;
  }

  if (loading) {
    return <IconoCentrado className="fa fa-spinner fa-spin fa-2x fa-fw" />;
  }

  const arrayItemsTiempo = llista.filter((el,index)=>index%4 === 0).map((el,index)=>{
      return(
        <Col>
          <ItemTiempo key={index+"-itemTiempo"} temp={el.main.temp} urlImg={"http://openweathermap.org/img/wn/"+el.weather[0].icon+"@4x.png"} dia={new Date(el.dt * 1000).toLocaleString().split(" ")[0]} />
        </Col>
      )
    
  })

  return (
    <Container fluid className={"p-3 text-center"}>
      <h3 className={"text-success mb-5"}><ins>Meteo Tiempo</ins></h3>
      <Row xs={5} className="justify-content-center">
        {arrayItemsTiempo}
      </Row>
    </Container>
  );
};

export default Meteo;
